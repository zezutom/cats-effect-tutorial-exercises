package tutorial.exercise01

import java.io._

import cats.effect.concurrent.Semaphore
import cats.effect.{Concurrent, IO, Resource}
import cats.implicits._

object CopyFile {

  def copy(origin: File, destination: File)(
    implicit concurrent: Concurrent[IO]): IO[Long] =
    for {
      guard <- Semaphore[IO](1)
      bufferSize = 1024 * 10
      count <- inputOutputStreams(origin, destination, guard).use {
        case (in, out) => guard.withPermit(transfer(in, out, bufferSize))
      }
    } yield count

  private def inputStream(f: File,
                          guard: Semaphore[IO]): Resource[IO, FileInputStream] =
    Resource.make {
      IO(new FileInputStream(f))
    } { inputStream =>
      // handle cancellation
      guard.withPermit {
        IO(inputStream.close()).handleErrorWith(_ => IO.unit)
      }
    }

  private def outputStream(
                            f: File,
                            guard: Semaphore[IO]): Resource[IO, FileOutputStream] =
    Resource.make {
      IO(new FileOutputStream(f))
    } { outputStream =>
      // handle cancellation
      guard.withPermit {
        IO(outputStream.close()).handleErrorWith(_ => IO.unit)
      }
    }

  private def inputOutputStreams(
                                  in: File,
                                  out: File,
                                  guard: Semaphore[IO]): Resource[IO, (FileInputStream, FileOutputStream)] =
    for {
      inputStream <- inputStream(in, guard)
      outputStream <- outputStream(out, guard)
    } yield (inputStream, outputStream)

  private def transfer(origin: InputStream,
                       destination: OutputStream,
                       bufferSize: Int): IO[Long] =
    for {
      buffer <- IO(new Array[Byte](bufferSize)) // allocated only when the IO is evaluated
      total <- transmit(origin, destination, buffer, 0L)
    } yield total

  private def transmit(origin: InputStream,
                       destination: OutputStream,
                       buffer: Array[Byte],
                       acc: Long): IO[Long] =
    for {
      amount <- IO(origin.read(buffer, 0, buffer.length))
      count <- if (amount > -1)
        IO(destination.write(buffer, 0, amount)) >> transmit(origin,
          destination,
          buffer,
          acc + amount)
      else IO.pure(acc) // Reached the end of stream, nothing to write
    } yield count
}

