package tutorial.exercise01

import java.io._
import cats.effect.concurrent.Semaphore
import cats.effect.{Concurrent, Resource, Sync}
import cats.implicits._

object CopyFilePolymorphic {

  def copy[F[_]: Concurrent](origin: File, destination: File): F[Long] =
    for {
      guard <- Semaphore[F](1)
      bufferSize = 1024 * 10
      count <- copy(origin, destination, bufferSize, guard)
    } yield count

  def copy[F[_]: Concurrent](origin: File,
                             destination: File,
                             bufferSize: Int,
                             guard: Semaphore[F]): F[Long] =
    for {
      count <- inputOutputStreams(origin, destination, guard).use {
        case (in, out) => guard.withPermit(transfer(in, out, bufferSize))
      }
    } yield count

  private def inputOutputStreams[F[_]: Concurrent](
      in: File,
      out: File,
      guard: Semaphore[F]): Resource[F, (FileInputStream, FileOutputStream)] =
    for {
      inputStream <- inputStream(in, guard)
      outputStream <- outputStream(out, guard)
    } yield (inputStream, outputStream)

  private def inputStream[F[_]: Sync](
      f: File,
      guard: Semaphore[F]): Resource[F, FileInputStream] =
    Resource.make {
      Sync[F].delay(new FileInputStream(f))
    } { inputStream =>
      // handle cancellation
      guard.withPermit {
        Sync[F].delay(inputStream.close()).handleErrorWith(_ => Sync[F].unit)
      }
    }

  private def outputStream[F[_]: Concurrent](
      f: File,
      guard: Semaphore[F]): Resource[F, FileOutputStream] =
    Resource.make {
      Sync[F].delay(new FileOutputStream(f))
    } { outputStream =>
      // handle cancellation
      guard.withPermit {
        Sync[F].delay(outputStream.close()).handleErrorWith(_ => Sync[F].unit)
      }
    }

  private def transfer[F[_]: Sync](origin: InputStream,
                                   destination: OutputStream,
                                   bufferSize: Int): F[Long] =
    for {
      buffer <- Sync[F].delay(new Array[Byte](bufferSize)) // allocated only when the IO is evaluated
      total <- transmit(origin, destination, buffer, 0L)
    } yield total

  private def transmit[F[_]: Sync](origin: InputStream,
                                   destination: OutputStream,
                                   buffer: Array[Byte],
                                   acc: Long): F[Long] =
    for {
      amount <- Sync[F].delay(origin.read(buffer, 0, buffer.length))
      count <- if (amount > -1)
        Sync[F].delay(destination.write(buffer, 0, amount)) >> transmit(
          origin,
          destination,
          buffer,
          acc + amount)
      else Sync[F].pure(acc) // Reached the end of stream, nothing to write
    } yield count
}
