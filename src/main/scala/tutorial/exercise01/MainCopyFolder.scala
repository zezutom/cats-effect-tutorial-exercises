package tutorial.exercise01

import java.io.File

import cats.effect.{ExitCode, IO, IOApp}

object MainCopyFolder extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    for {
      _ <- if (args.length < 2)
        IO.raiseError(
          new IllegalArgumentException(
            "need origin and destination directories"))
      else IO.unit
      origin = new File(args.head)
      destination = new File(args(1))
      _ <- if (origin == destination)
        IO.raiseError(
          new IllegalArgumentException("origin and destination must differ!"))
      else IO.unit
      count <- CopyFolder.copy[IO](origin, destination)
      _ <- IO(println(s"$count bytes copied from $origin to $destination"))
    } yield ExitCode.Success
  }
}
