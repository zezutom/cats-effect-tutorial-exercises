package tutorial.exercise01

import java.io._

import cats.effect.concurrent.Semaphore
import cats.effect.{Concurrent, Resource, Sync}
import cats.implicits._

object CopyFolder {

  def copy[F[_]: Concurrent](origin: File, destination: File): F[Long] = {
    for {
      guard <- Semaphore[F](1)
      bufferSize = 1024 * 10
      count <- copyFolder(origin, destination, bufferSize, guard)(
        Concurrent[F].pure(0L))
    } yield count
  }

  private def copyFolder[F[_]: Concurrent](
      origin: File,
      destination: File,
      bufferSize: Int,
      guard: Semaphore[F])(acc: F[Long]): F[Long] = {
    if (origin.isDirectory) {
      for {
        total <- origin.list().foldLeft(acc) {
          case (sum, file) =>
            for {
              runningTotal <- sum
              count <- createFolders(origin, destination, file, guard).use {
                case (in, out) => copyFolder(in, out, bufferSize, guard)(acc)
              }
            } yield runningTotal + count
        }
      } yield total
    } else {
      CopyFilePolymorphic.copy(origin, destination, bufferSize, guard)
    }
  }

  private def createFolders[F[_]: Sync](
      origin: File,
      destination: File,
      path: String,
      guard: Semaphore[F]): Resource[F, (File, File)] =
    for {
      in <- createFolder(origin, path, guard)
      out <- createFolder(destination, path, guard)
    } yield {
      (in, out)
    }

  private def createFolder[F[_]: Sync](base: File,
                                       path: String,
                                       guard: Semaphore[F]): Resource[F, File] =
    Resource.liftF {
      guard.withPermit {
        Sync[F].delay {
          if (!base.exists()) base.mkdir()
          new File(base, path)
        }
      }
    }
}
