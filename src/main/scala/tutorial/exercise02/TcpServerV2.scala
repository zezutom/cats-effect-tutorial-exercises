package tutorial.exercise02

import java.io._
import java.net.{ServerSocket, Socket}

import cats.effect.ExitCase.{Canceled, Completed, Error}
import cats.effect._
import cats.effect.concurrent.MVar
import cats.effect.syntax.all._
import cats.implicits._

/**
  * Server that listens on a given port, and for each new connected client it spawns a new fiber to attend it. This
  *  fiber will just send back any line sent by the client ('echo'). When a client sends an empty line, the connection
  *  with that client is closed. When a client sends a "STOP" message, the server is shut down and all client fibers are gracefully terminated.
  *
  *  credit: https://github.com/lrodero/cats-effect-tutorial/blob/master/src/main/scala/catsEffectTutorial/EchoServerV1_Simple.scala
  */
object TcpServerV2 extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    IO(new ServerSocket(args.headOption.map(_.toInt).getOrElse(5000))).bracket {
      serverSocket =>
        server[IO](serverSocket) >> IO.pure(ExitCode.Success)
    } { serverSocket =>
      close[ServerSocket, IO](serverSocket) >> IO(println("Server finished."))
    }

  def server[F[_]: Concurrent](serverSocket: ServerSocket): F[Unit] =
    for {
      stopFlag <- MVar[F].empty[Unit]
      serverFiber <- serve(serverSocket, stopFlag).start // Start the server in its own fiber
      _ <- stopFlag.read // Blocked until 'stopFlag.put()' is called
      _ <- serverFiber.cancel.start // Stop the server
    } yield ExitCode.Success

  def echoProtocol[F[_]: Sync](clientSocket: Socket,
                               stopFlag: MVar[F, Unit]): F[Unit] =
    readerWriter(clientSocket).use {
      case (reader, writer) => loop(reader, writer, stopFlag)
    }

  // Serve now requires access to the stopFlag, it will use it to signal the server must stop.
  def serve[F[_]: Concurrent](serverSocket: ServerSocket,
                              stopFlag: MVar[F, Unit]): F[Unit] =
    for {
      clientSocket <- Sync[F]
        .delay(serverSocket.accept())
        .bracketCase { clientSocket =>
          echoProtocol(clientSocket, stopFlag)
            .guarantee(close(clientSocket)) // Ensure the socket gets closed no matter what
            .start >> Sync[F].pure(clientSocket) // Client attended by its own fiber, socket is returned
        } { (clientSocket, exit) =>
          exit match {
            case Completed =>
              Sync[F].unit // The client socket will only be closed when the echoProtocol is done
            case Error(_) | Canceled =>
              close(clientSocket) // Explicitly dispose of the client socket
          }
        }
      _ <- (stopFlag.read >> close(clientSocket)).start // Another Fiber to cancel the client when stopFlag is set (by any of the connected clients, e.g. "STOP" message)
      _ <- serve(serverSocket, stopFlag)
    } yield ()

  private def readerWriter[F[_]: Sync](
      clientSocket: Socket): Resource[F, (BufferedReader, BufferedWriter)] =
    for {
      reader <- reader(clientSocket)
      writer <- writer(clientSocket)
    } yield (reader, writer)

  private def reader[F[_]: Sync](
      clientSocket: Socket): Resource[F, BufferedReader] =
    createResource(
      clientSocket,
      (s: Socket) =>
        new BufferedReader(new InputStreamReader(s.getInputStream)))

  private def writer[F[_]: Sync](
      clientSocket: Socket): Resource[F, BufferedWriter] =
    createResource(
      clientSocket,
      (s: Socket) => new BufferedWriter(new PrintWriter(s.getOutputStream)))

  private def loop[F[_]: Sync](reader: BufferedReader,
                               writer: BufferedWriter,
                               stopFlag: MVar[F, Unit]): F[Unit] =
    for {
      lineEither <- Sync[F]
        .delay(reader.readLine())
        .attempt // Since the client fiber might have been terminated, "readLine()" now must be checked for exceptions
      _ <- lineEither match {
        case Right(line) =>
          line match {
            case "STOP" =>
              stopFlag.put(())
            case "" =>
              Sync[F].unit
            case _ =>
              Sync[F].delay {
                writer.write(line)
                writer.newLine()
                writer.flush()
              } >> loop(reader, writer, stopFlag)
          }
        case Left(e) =>
          for {
            isEmpty <- stopFlag.isEmpty
            _ = if (!isEmpty)
              Sync[F].unit // The stopFlag has been set, we are done
            else Sync[F].raiseError(e) // This is a genuine exception, throw it
          } yield ()
      }
    } yield ()

  private def createResource[A, B <: Closeable, F[_]: Sync](
      a: A,
      fab: A => B): Resource[F, B] =
    Resource.make {
      Sync[F].delay(fab(a))
    } { resource =>
      close(resource)
    }

  private def close[A <: Closeable, F[_]: Sync](a: A): F[Unit] =
    Sync[F].delay(a.close()).handleErrorWith(_ => Sync[F].unit)
}
