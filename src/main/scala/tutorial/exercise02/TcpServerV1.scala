package tutorial.exercise02

import java.io._
import java.net.{ServerSocket, Socket}

import cats.effect.ExitCase.{Canceled, Completed, Error}
import cats.effect._
import cats.effect.syntax.all._
import cats.implicits._

/**
  * Server that listens on a given port, and for each new connected client it spawns a new fiber to attend it. This
  *  fiber will just send back any line sent by the client ('echo'). When a client sends an empty line, the connection
  *  with that client is closed.
  *
  *  credit: https://github.com/lrodero/cats-effect-tutorial/blob/master/src/main/scala/catsEffectTutorial/EchoServerV1_Simple.scala
  */
object TcpServerV1 extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    IO(new ServerSocket(args.headOption.map(_.toInt).getOrElse(5000))).bracket {
      serverSocket =>
        serve[IO](serverSocket) >> IO.pure(ExitCode.Success)
    } { serverSocket =>
      close[ServerSocket, IO](serverSocket) >> IO(println("Server finished."))
    }

  def echoProtocol[F[_]: Sync](clientSocket: Socket): F[Unit] =
    readerWriter(clientSocket).use {
      case (reader, writer) => loop(reader, writer)
    }

  def serve[F[_]: Concurrent](serverSocket: ServerSocket): F[Unit] =
    for {
      _ <- Sync[F]
        .delay(serverSocket.accept())
        .bracketCase { clientSocket =>
          echoProtocol(clientSocket)
            .guarantee(close(clientSocket)) // Ensure the socket gets closed no matter what
            .start // Will run in its own fiber, thus not blocking the main loop
        } { (clientSocket, exit) =>
          exit match {
            case Completed =>
              Sync[F].unit // The client socket will only be closed when the echoProtocol is done
            case Error(_) | Canceled =>
              close(clientSocket) // Explicitly dispose of the client socket
          }
        }
      _ <- serve(serverSocket)
    } yield ()

  private def readerWriter[F[_]: Sync](
      clientSocket: Socket): Resource[F, (BufferedReader, BufferedWriter)] =
    for {
      reader <- reader(clientSocket)
      writer <- writer(clientSocket)
    } yield (reader, writer)

  private def reader[F[_]: Sync](
      clientSocket: Socket): Resource[F, BufferedReader] =
    createResource(
      clientSocket,
      (s: Socket) =>
        new BufferedReader(new InputStreamReader(s.getInputStream)))

  private def writer[F[_]: Sync](
      clientSocket: Socket): Resource[F, BufferedWriter] =
    createResource(
      clientSocket,
      (s: Socket) => new BufferedWriter(new PrintWriter(s.getOutputStream)))

  private def loop[F[_]: Sync](reader: BufferedReader,
                               writer: BufferedWriter): F[Unit] =
    for {
      line <- Sync[F].delay(reader.readLine())
      _ <- line match {
        case "" =>
          Sync[F].unit
        case _ =>
          Sync[F].delay {
            writer.write(line)
            writer.newLine()
            writer.flush()
          } >> loop(reader, writer)
      }
    } yield ()

  private def createResource[A, B <: Closeable, F[_]: Sync](
      a: A,
      fab: A => B): Resource[F, B] =
    Resource.make {
      Sync[F].delay(fab(a))
    } { resource =>
      close(resource)
    }

  private def close[A <: Closeable, F[_]: Sync](a: A): F[Unit] =
    Sync[F].delay(a.close()).handleErrorWith(_ => Sync[F].unit)
}
